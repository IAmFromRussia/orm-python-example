﻿$containerName = "postgres_container"
$user = "postgres"
$password = "postgres"
$port = 5432
$image = "postgres:latest"

docker run -d `
    --name $containerName `
    -e "POSTGRES_USER=$user" `
    -e "POSTGRES_PASSWORD=$password" `
    -p 5432:5432 `
    $image