﻿from typing import List
from sqlalchemy.orm import Session
from Entities import Entity
from Interfaces.IRepository import IRepository


class EntityRepository(IRepository):
	
	entity: object = NotImplemented
	db: object = NotImplemented
	
	def __init__(self, db: Session):
		self.db = db

	def create(self, entity):
		self.db.add(entity)
		self.db.commit()

	def update(self, entity):
		self.db.commit()

	def delete(self, entity):
		self.db.delete(entity)
		self.db.commit()

	def get_by_id(self, entity_id: int):
		return self.db.query(Entity.Entity).filter(Entity.Entity.id==entity_id).one()

	def get_all(self) -> List:
		return self.db.query(Entity).all()
