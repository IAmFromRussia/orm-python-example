﻿from abc import abstractmethod
from typing import List


class IRepository:
	@abstractmethod
	def create(self, entity):
		pass

	@abstractmethod
	def update(self, entity):
		pass

	@abstractmethod
	def delete(self, entity):
		pass

	@abstractmethod
	def get_by_id(self, entity_id):
		pass

	@abstractmethod
	def get_all(self) -> List:
		pass