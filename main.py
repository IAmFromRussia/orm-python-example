﻿from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

from Entities.Entity import Entity, Base
from Repositories.EnitityRepository import EntityRepository

# PostgreSQL connection string
# Replace 'your_postgres_user', 'your_postgres_password', 'your_postgres_host', 'your_postgres_port', and 'your_postgres_db'
# with your actual PostgreSQL credentials and database details
connection_string = "postgresql://postgres:postgres@localhost:5432"

# Create a database connection and session
engine = create_engine(connection_string)
Session = sessionmaker(bind=engine)
session = Session()

# Create the tables in the database if they don't exist
Base.metadata.create_all(engine)

# Create an instance of the repository object
entity_repo = EntityRepository(session)

# Add a new object to the database
new_entity = Entity(name='Olsi')
entity_repo.create(new_entity)


just_added_entity = entity_repo.get_by_id(12)

# Delete an object from the database

entity_to_delete = entity_repo.get_by_id(just_added_entity.id)

if entity_to_delete:
    entity_repo.delete(entity_to_delete)
else:
    print("Entity not found.")
